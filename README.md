# The EPIC Monday-Morning Integration Toolkit

[![pipeline status](https://codebase.helmholtz.cloud/emmitools/emmi/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/emmitools/emmi/-/commits/master)
[![coverage report](https://codebase.helmholtz.cloud/emmitools/emmi/badges/master/coverage.svg)](https://codebase.helmholtz.cloud/emmitools/emmi/-/commits/master)
[![Latest Release](https://codebase.helmholtz.cloud/emmitools/emmi/-/badges/release.svg)](https://codebase.helmholtz.cloud/emmitools/emmi/-/releases)
[![pylint](https://codebase.helmholtz.cloud/emmitools/emmi/-/badges/pylint.svg)](https://codebase.helmholtz.cloud/emmitools/emmi/-/lint/)

EMMItool is aiming to be the "Swiss army knife" for rapid ad-hoc
integration of
scientific instrumentation into [EPICS](https://epics.anl.gov/)-based
[beamline control sytems](https://blueskyproject.io/).

But currently, it's more like "the chaotic toolbox of a confused craftsman".

See [the documentation](./doc/index.md) for a rapid introduction about
the structure and what's in the box.

